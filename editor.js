/* Модуль редактора кода
 * ---------------------
 * 
 * (c) 2019, ТяФ/ка (tyafffka)
 * 
 * Данное программное обеспечение предоставляется "как есть", без каких-либо
 * явных или подразумеваемых гарантий. В любых случаях его автор(ы) снимает(ют)
 * с себя ответственность за любой ущерб, связанный с использованием данного
 * программного обеспечения.
 * 
 * Данное программное обеспечение разрешается использовать кому угодно и в любых
 * целях, включая его коммерческое применение, а также изменять и распространять
 * его свободно при условии соблюдения следующих ограничений:
 * 
 * 1. Происхождение данного программного обеспечения не должно быть искажено; Вы
 *    не в праве утверждать, что это Вы написали оригинальное программное
 *    обеспечение. Если Вы используете данное программное обеспечение в своём
 *    продукте, то упоминание об этом в документации проекта было бы
 *    признательно, но не обязательно.
 * 
 * 2. Изменённые версии исходных файлов должны быть явно обозначены как таковые
 *    и не должны выдаваться за оригинальное программное обеспечение.
 * 
 * 3. Данное уведомление о лицензии не должно убираться или изменяться при любом
 *    распространении исходных текстов. */


/* Количество символов пробела в одном отступе. */
const EDITOR_TAB_STOPS = 4;

// --- *** ---


/* Сохраняет код из редактора в локальное хранилище. */
function saveEdit()
{
    window.localStorage.setItem('cockroachTheGame::savedEdit', editor.value);
}


/* Загружает в редактор ранее сохранённый код. */
function loadSavedEdit()
{
    let saved = window.localStorage.getItem('cockroachTheGame::savedEdit');
    if(! saved) return;
    
    editor.value = saved; adjustLineNumbers();
}


/* Генерирует номера строк в колонке слева
 * в соответствии с количеством строк в редакторе. */
function adjustLineNumbers()
{
    let new_lines_count = (editor.value.match(/\n/g) || []).length + 1;
    if(editor.rows != new_lines_count)
    {
        editor.rows = new_lines_count;
        
        editorLineNumbers.innerHTML = "";
        for(let i = 1; i <= new_lines_count; ++i)
            editorLineNumbers.innerHTML += i + "<br>";
    }
}


/* Обрабатывает нажатия служебных клавиш в поле редактора. */
function onEditKeys(e)
{
    if(e.key == 'Tab')
    {
        if(! e.ctrlKey && ! e.altKey && ! e.metaKey)
        {
            if(e.shiftKey) _on_edit_key_shift_tab(e.target);
            else _on_edit_key_tab(e.target);
            
            e.preventDefault();
        }
    }
    else if(e.key == 'Enter')
    {
        if(e.ctrlKey && ! e.shiftKey && ! e.altKey && ! e.metaKey)
        {
            runGame(); e.preventDefault();
        }
    }
}

// --- *** ---


function _on_edit_key_tab(target_)
{
    let sel_pos = target_.selectionStart;
    let value = target_.value;
    let line_start = sel_pos;
    
    while(line_start > 0)
    {
        let ch = value[--line_start];
        if(ch == '\n') {   ++line_start; break;   }
        else if(ch != ' ') {   return;   }
    }
    
    while(value[sel_pos] == ' ') ++sel_pos;
    
    let text_before = value.slice(0, sel_pos);
    let text_after = value.slice(sel_pos);
    let text_inserted = ' '; ++sel_pos;
    
    while((sel_pos - line_start) % EDITOR_TAB_STOPS != 0)
    {
        text_inserted += ' '; ++sel_pos;
    }
    
    target_.value = text_before + text_inserted + text_after;
    target_.selectionStart = sel_pos;
    target_.selectionEnd = sel_pos;
}


function _on_edit_key_shift_tab(target_)
{
    let sel_pos = target_.selectionStart;
    let value = target_.value;
    let line_start = sel_pos;
    
    while(line_start > 0)
    {
        let ch = value[--line_start];
        if(ch == '\n') {   ++line_start; break;   }
        else if(ch != ' ') {   sel_pos = line_start;   }
    }
    
    while(value[sel_pos] == ' ') ++sel_pos;
    if(sel_pos <= line_start) return;
    
    let text_after = value.slice(sel_pos);
    
    do {   --sel_pos;   }
    while(sel_pos > line_start && (sel_pos - line_start) % EDITOR_TAB_STOPS != 0);
    
    target_.value = value.slice(0, sel_pos) + text_after;
    target_.selectionStart = sel_pos;
    target_.selectionEnd = sel_pos;
}
