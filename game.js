/* Модуль модели игры
 * ------------------
 * 
 * Модель игры представляет из себя крохотную виртуальную машину, в систему
 * команд которой входят в том числе и движения персонажа. После исполнения
 * каждой такой команды отрисовывается изменённое игровое поле и делается
 * небольшая задержка.
 * 
 * Состояние игры хранится одним объектом и состоит из следующих полей:
 * - `player_pos` -- массив из трёх элементов -- координаты персонажа на поле
 *   и его поворот;
 * - `map` -- двумерный массив ячеек прямоугольного игрового поля, значение --
 *   номер спрайта;
 * - `program` -- массив инструкций исполняемого байт-кода;
 * - `ip` -- счётчик команд -- индекс текущей инструкции в массиве;
 * - `stack` -- стековая память (массив) для программы.
 * 
 * В начальном состоянии присутствуют только `player_pos` и `map`.
 * 
 * ---
 * 
 * (c) 2019, ТяФ/ка (tyafffka)
 * 
 * Данное программное обеспечение предоставляется "как есть", без каких-либо
 * явных или подразумеваемых гарантий. В любых случаях его автор(ы) снимает(ют)
 * с себя ответственность за любой ущерб, связанный с использованием данного
 * программного обеспечения.
 * 
 * Данное программное обеспечение разрешается использовать кому угодно и в любых
 * целях, включая его коммерческое применение, а также изменять и распространять
 * его свободно при условии соблюдения следующих ограничений:
 * 
 * 1. Происхождение данного программного обеспечения не должно быть искажено; Вы
 *    не в праве утверждать, что это Вы написали оригинальное программное
 *    обеспечение. Если Вы используете данное программное обеспечение в своём
 *    продукте, то упоминание об этом в документации проекта было бы
 *    признательно, но не обязательно.
 * 
 * 2. Изменённые версии исходных файлов должны быть явно обозначены как таковые
 *    и не должны выдаваться за оригинальное программное обеспечение.
 * 
 * 3. Данное уведомление о лицензии не должно убираться или изменяться при любом
 *    распространении исходных текстов. */


/* Величина задержки при отрисовке состояния. */
const GAME_STEP_DELAY = 800 /*ms*/;

/* Начальное состояние игры. */
const INITIAL_GAME_STATE = {
  player_pos: [0, 0, SpriteRotation.RIGHT],
  map: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0],
    [0, 1, 0, 0, 1, 0, 0, 1, 0],
    [0, 1, 0, 0, 1, 0, 0, 1, 0],
    [0, 1, 0, 0, 1, 0, 0, 1, 0],
  ]
};

/* Размеры игрового поля. */
const GAME_CELLS_X_COUNT = INITIAL_GAME_STATE.map[0].length;
const GAME_CELLS_Y_COUNT = INITIAL_GAME_STATE.map.length;

/* Объект состояния игры. */
var gameModel = null;

// --- *** ---


/* Запускает исполнение кода из редактора от начального состояния игры. */
function runGame()
{
    if(gameModel) gameModel.stop();
    
    drawState(INITIAL_GAME_STATE);
    clearNotes();
    
    let translated = translateCode(editor.value);
    if(translated)
    {
        gameModel = new _GameModel(INITIAL_GAME_STATE, translated);
        gameModel.run_step();
    }
    else
    {   gameModel = null;   }
}


/* Прерывает исполнение кода и возвращает игру в начальное состояние. */
function interruptGame()
{
    if(! gameModel) return;
    
    gameModel.stop(); gameModel = null;
    
    drawState(INITIAL_GAME_STATE);
    clearNotes();
}

// --- *** ---


function _GameModel(initial_state_, program_)
{
    this.player_pos = deepCopy(initial_state_.player_pos);
    this.map = deepCopy(initial_state_.map);
    this.program = program_;
    this.ip = 0;
    this.stack = [];
    this.step_handle = null;
}


_GameModel.prototype.run_step = function()
{
    this.step_handle = window.setTimeout(()=>this.do_step(), GAME_STEP_DELAY);
}


_GameModel.prototype.stop = function()
{
    if(this.step_handle)
    {
        window.clearTimeout(this.step_handle); this.step_handle = null;
    }
}


_GameModel.prototype.get_cmd_data = function(is_jmp_point_)
{
    let data = this.program[++this.ip];
    if(typeof(data) != "number")
    {
        writeErrorNote("BYTE: Повреждённые бинарные данные! " + this.ip + ": " + data);
        return null;
    }
    else if(is_jmp_point_ && (data < 0 || this.program.length <= data))
    {
        writeErrorNote("BYTE: Позиция перехода за пределами допустимого диапазона! " + this.ip + ": " + data + " | " + this.program.length);
        return null;
    }
    return data;
}


_GameModel.prototype.do_step = function()
{
    while(this.ip < this.program.length)
    {
        let cmd = this.program[this.ip], data;
        let is_visual_step = false;
        
        switch(cmd)
        {
        case OpCode.NOOP: break;
            
        case OpCode.PUSH_VALUE:
            if((data = this.get_cmd_data(false)) === null) return;
            this.stack.push(data);
            break;
            
        case OpCode.GOTO:
            if((data = this.get_cmd_data(true)) === null) return;
            this.ip = data;
            continue;
            
        case OpCode.CALL:
            if((data = this.get_cmd_data(true)) === null) return;
            this.stack.push(this.ip + 1);
            this.ip = data;
            continue;
            
        case OpCode.RETURN:
            data = this.stack.pop();
            if(typeof(data) != "number")
            {
                writeErrorNote("BYTE: Стек повреждён или пуст (при возврате)! " + this.ip + ": " + data);
                return;
            }
            this.ip = data;
            continue;
            
        case OpCode.COUNTDOWN_AND_GOTO:
            data = this.stack.pop();
            if(typeof(data) != "number")
            {
                writeErrorNote("BYTE: Стек повреждён или пуст (при итерации)! " + this.ip + ": " + data);
                return;
            }
            
            if(data > 0)
            {
                this.stack.push(data - 1);
                ++this.ip; // skip additional data
                break;
            }
            
            if((data = this.get_cmd_data(true)) === null) return;
            this.ip = data;
            continue;
            
        case OpCode.PLAYER_MOVE_UP:
        case OpCode.PLAYER_MOVE_RIGHT:
        case OpCode.PLAYER_MOVE_DOWN:
        case OpCode.PLAYER_MOVE_LEFT:
            if(! this.player_move(cmd)) return;
            is_visual_step = true;
            break;
            
        default:
            writeErrorNote("BYTE: Неизвестная бинарная команда! " + this.ip + ": " + cmd);
            return;
        }
        
        ++this.ip;
        if(is_visual_step) break;
    }
    
    drawState(this);
    
    if(this.ip < this.program.length)
    {   this.run_step();   }
    else
    {   this.step_handle = null; writeCompleteNote();   }
}


_GameModel.prototype.player_move = function(cmd_)
{
    let dx, dy, rot;
    switch(cmd_)
    {
    case OpCode.PLAYER_MOVE_UP:
        dx = 0;  dy = -1; rot = SpriteRotation.UP;
        break;
    case OpCode.PLAYER_MOVE_RIGHT:
        dx = 1;  dy = 0; rot = SpriteRotation.RIGHT;
        break;
    case OpCode.PLAYER_MOVE_DOWN:
        dx = 0;  dy = 1; rot = SpriteRotation.DOWN;
        break;
    case OpCode.PLAYER_MOVE_LEFT:
        dx = -1; dy = 0; rot = SpriteRotation.LEFT;
        break;
    default: return false;
    }
    
    let p_sprite = null, trans_sprite = SpriteType.EMPTY;
    let new_x = this.player_pos[0] + dx;
    let new_y = this.player_pos[1] + dy;
    
    while(0 <= new_x && new_x < GAME_CELLS_X_COUNT &&
          0 <= new_y && new_y < GAME_CELLS_Y_COUNT)
    {
        p_sprite = this.map[new_y][new_x];
        this.map[new_y][new_x] = trans_sprite;
        
        if(p_sprite == SpriteType.EMPTY) break;
        
        trans_sprite = p_sprite;
        new_x += dx; new_y += dy;
    }
    
    if(p_sprite !== null)
    {
        this.player_pos[0] += dx;
        this.player_pos[1] += dy;
    }
    this.player_pos[2] = rot;
    return true;
}
