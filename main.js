/* (c) 2019, ТяФ/ка (tyafffka)
 * 
 * Данное программное обеспечение предоставляется "как есть", без каких-либо
 * явных или подразумеваемых гарантий. В любых случаях его автор(ы) снимает(ют)
 * с себя ответственность за любой ущерб, связанный с использованием данного
 * программного обеспечения.
 * 
 * Данное программное обеспечение разрешается использовать кому угодно и в любых
 * целях, включая его коммерческое применение, а также изменять и распространять
 * его свободно при условии соблюдения следующих ограничений:
 * 
 * 1. Происхождение данного программного обеспечения не должно быть искажено; Вы
 *    не в праве утверждать, что это Вы написали оригинальное программное
 *    обеспечение. Если Вы используете данное программное обеспечение в своём
 *    продукте, то упоминание об этом в документации проекта было бы
 *    признательно, но не обязательно.
 * 
 * 2. Изменённые версии исходных файлов должны быть явно обозначены как таковые
 *    и не должны выдаваться за оригинальное программное обеспечение.
 * 
 * 3. Данное уведомление о лицензии не должно убираться или изменяться при любом
 *    распространении исходных текстов. */


var spritePlayer;
var spriteEmpty;
var spriteBlock;

var editor;
var editorLineNumbers;
var errorNote;
var completeNote;
var outputContext;

// --- *** ---


function init()
{
    // Find interested elements
    
    spritePlayer = document.getElementById('sprite_player');
    spriteEmpty = document.getElementById('sprite_empty');
    spriteBlock = document.getElementById('sprite_block');
    
    let button_save = document.getElementById('button_save');
    let button_run = document.getElementById('button_run');
    let button_interrupt = document.getElementById('button_interrupt');
    
    editor = document.getElementById('editor');
    editorLineNumbers = document.getElementById('editor_line_numbers');
    errorNote = document.getElementById('error_note');
    completeNote = document.getElementById('complete_note');
    
    let output_canvas = document.getElementById('output_canvas');
    
    // Set parameters to the elements
    
    button_save.addEventListener('click', saveEdit);
    button_run.addEventListener('click', runGame);
    button_interrupt.addEventListener('click', interruptGame);
    editor.addEventListener('input', adjustLineNumbers);
    editor.addEventListener('keydown', onEditKeys);
    
    GAME_CELL_SIZE = spriteEmpty.width;
    
    output_canvas.width = GAME_CELL_SIZE * GAME_CELLS_X_COUNT;
    output_canvas.height = GAME_CELL_SIZE * GAME_CELLS_Y_COUNT;
    
    outputContext = output_canvas.getContext('2d', {alpha: false});
    
    drawState(INITIAL_GAME_STATE);
    loadSavedEdit();
}


// --- Utils: ---

function deepCopy(obj)
{
    let copy;
    if(Array.isArray(obj))
    {
        copy = [];
        obj.forEach((v, i)=>
        {
            copy[i] = (v !== null && typeof(v) == "object")? deepCopy(v) : v;
        });
    }
    else
    {
        copy = {}; copy.__proto__ = obj.__proto__;
        for(let p in obj)
        {
            let v = obj[p];
            copy[p] = (v !== null && typeof(v) == "object")? deepCopy(v) : v;
        }
    }
    return copy;
}


HTMLTextAreaElement.prototype.insertAtCaret = function(text) // FIXME:UNUSED
{
    text = text || "";
    let start_pos = this.selectionStart;
    let end_pos = this.selectionEnd;
    
    this.value = this.value.slice(0, start_pos) + text + this.value.slice(end_pos);
    this.selectionStart = start_pos + text.length;
    this.selectionEnd = start_pos + text.length;
};
