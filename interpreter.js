/* Модуль интерпретатора кода
 * --------------------------
 * 
 * (c) 2019, ТяФ/ка (tyafffka)
 * 
 * Данное программное обеспечение предоставляется "как есть", без каких-либо
 * явных или подразумеваемых гарантий. В любых случаях его автор(ы) снимает(ют)
 * с себя ответственность за любой ущерб, связанный с использованием данного
 * программного обеспечения.
 * 
 * Данное программное обеспечение разрешается использовать кому угодно и в любых
 * целях, включая его коммерческое применение, а также изменять и распространять
 * его свободно при условии соблюдения следующих ограничений:
 * 
 * 1. Происхождение данного программного обеспечения не должно быть искажено; Вы
 *    не в праве утверждать, что это Вы написали оригинальное программное
 *    обеспечение. Если Вы используете данное программное обеспечение в своём
 *    продукте, то упоминание об этом в документации проекта было бы
 *    признательно, но не обязательно.
 * 
 * 2. Изменённые версии исходных файлов должны быть явно обозначены как таковые
 *    и не должны выдаваться за оригинальное программное обеспечение.
 * 
 * 3. Данное уведомление о лицензии не должно убираться или изменяться при любом
 *    распространении исходных текстов. */


/* Набор ключевых слов, используемых интерпретатором. */
const KeyWord = {
    REPEAT: "повтор",
    SUB: "группа",
    END: "конец",
    UP: "вверх",
    RIGHT: "вправо",
    DOWN: "вниз",
    LEFT: "влево",
};

/* Набор кодов команд виртуальной машины. */
const OpCode = {
    /* Нет действия (заполнитель) */
    NOOP: 0,
    /* Поместить в стек следующее значение */
    PUSH_VALUE: 1,
    /* Перейти к инструкции со следующим индексом */
    GOTO: 2,
    /* Поместить в стек индекс следующей по порядку инструкции
     * и перейти к инструкции со следующим индексом */
    CALL: 3,
    /* Извлечь из стека индекс инструкции и перейти к ней */
    RETURN: 4,
    /* Уменьшить на единицу значение на вершине стека, или,
     * если оно достигло нуля, перейти к инструкции со следующим индексом */
    COUNTDOWN_AND_GOTO: 5,
    /* Шаг персонажа вверх */
    PLAYER_MOVE_UP: 100,
    /* Шаг персонажа враво */
    PLAYER_MOVE_RIGHT: 101,
    /* Шаг персонажа вниз */
    PLAYER_MOVE_DOWN: 102,
    /* Шаг персонажа влево */
    PLAYER_MOVE_LEFT: 103,
};

/* Выводить ли байт-код в консоль после разбора. */
const IS_LOG_ASSEMBLED = true;

// --- *** ---


/* Разбирает переданный исходный код и возвращает байт-код
 * для виртуальной машины. Если разбор не удался, возвращает `null`. */
function translateCode(source_code_)
{
    let translator = new _Translator();
    
    let i = 1;
    for(const line of source_code_.split('\n'))
    {
        for(const token of (line.match(/\S+/g) || []))
        {
            translator.tokens.push(token);
            translator.token_lines.push(i);
        }
        ++i;
    }
    translator.token_lines.push(i);
    
    // ---
    
    if(! translator.make_code()) return null;
    
    if(translator.token_i < translator.tokens.length)
    {
        writeErrorNote("Ошибка синтаксиса! Строка " + translator.token_lines[translator.token_i]);
        return null;
    }
    
    if(translator.program.length == 0)
    {
        writeErrorNote("Программа пустая!");
        return null;
    }
    
    translator.program.push(OpCode.NOOP);
    
    if(IS_LOG_ASSEMBLED)
    {
        console.group("Assembled", "|", new Date().toTimeString());
        console.log(disassembleCode(translator.program));
        console.groupEnd();
    }
    return translator.program;
}


/* Возвращает строковое представление байт-кода из массива `program_`. */
function disassembleCode(program_)
{
    if(! program_ || ! program_.length) return "<empty>";
    
    let lines = [], i = 0;
    while(i < program_.length)
    {
        let begin_i = i, cmd = program_[i++];
        
        let cmd_name = null;
        for(let p in OpCode) if(cmd === OpCode[p])
        {
           cmd_name = p; break;
        }
        if(cmd_name === null) cmd_name = "<unknown>";
        
        if(cmd === OpCode.PUSH_VALUE || cmd === OpCode.GOTO ||
           cmd === OpCode.CALL || cmd === OpCode.COUNTDOWN_AND_GOTO)
        {
            let arg = program_[i++];
            cmd_name += ", " + (Number.isInteger(arg) ? arg : "<???>");
        }
        
        lines.push(begin_i + ": " + cmd_name);
    }
    return lines.join('\n');
}

// --- *** ---


function _Translator()
{
    this.tokens = [];
    this.token_lines = [];
    this.token_i = 0;
    this.subs = {}; // {sub_name -> begin_address}
    this.program = [];
}


_Translator.prototype.make_error = function(msg_)
{
    writeErrorNote("Ошибка! Строка " + this.token_lines[this.token_i] + ": " + msg_);
    return false;
}


_Translator.prototype.make_code = function()
{
    while(this.token_i < this.tokens.length)
    {
        let token = this.tokens[this.token_i];
        
        switch(token)
        {
        case KeyWord.REPEAT:
            ++this.token_i;
            if(! this.make_repeat()) return false;
            continue;
            
        case KeyWord.SUB:
            ++this.token_i;
            if(! this.make_sub()) return false;
            continue;
            
        case KeyWord.END:
            return true;
            
        case KeyWord.UP:
            this.program.push(OpCode.PLAYER_MOVE_UP);
            break;
            
        case KeyWord.RIGHT:
            this.program.push(OpCode.PLAYER_MOVE_RIGHT);
            break;
            
        case KeyWord.DOWN:
            this.program.push(OpCode.PLAYER_MOVE_DOWN);
            break;
            
        case KeyWord.LEFT:
            this.program.push(OpCode.PLAYER_MOVE_LEFT);
            break;
            
        default:
            let sub_addr = this.subs[token];
            if(Number.isInteger(sub_addr))
            {
                this.program.push(OpCode.CALL, sub_addr); break;
            }
            
            return this.make_error("неизвестная команда '" + token + "'");
        }
        
        ++this.token_i;
    }
    return true;
}


_Translator.prototype.make_repeat = function()
{
    if(this.token_i >= this.tokens.length)
        return this.make_error("неожиданный конец на команде '" + KeyWord.REPEAT + "'");
    
    let n_repeats = Number(this.tokens[this.token_i]);
    if(! Number.isInteger(n_repeats))
        return this.make_error("неверный синтаксис команды '" + KeyWord.REPEAT + "'");
    
    let loop_addr = this.program.push(OpCode.PUSH_VALUE, n_repeats);
    let replace_addr = this.program.push(OpCode.COUNTDOWN_AND_GOTO, 0) - 1;
    
    ++this.token_i;
    if(! this.make_code()) return false;
    
    if(this.tokens[this.token_i] != KeyWord.END)
        return this.make_error("неверный синтаксис команды '" + KeyWord.REPEAT + "'");
    
    this.program[replace_addr] = this.program.push(OpCode.GOTO, loop_addr);
    
    ++this.token_i;
    return true;
}


_Translator.prototype.make_sub = function()
{
    if(this.token_i >= this.tokens.length)
        return this.make_error("неожиданный конец на команде '" + KeyWord.SUB + "'");
    
    let sub_name = this.tokens[this.token_i];
    if(! /^(?:\w|\p{L})+$/ug.test(sub_name))
        return this.make_error("неправильное имя группы");
    
    let sub_addr = this.program.push(OpCode.GOTO, 0);
    let replace_addr = sub_addr - 1;
    
    ++this.token_i;
    if(! this.make_code()) return false;
    
    if(this.tokens[this.token_i] != KeyWord.END)
        return this.make_error("неверный синтаксис команды '" + KeyWord.SUB + "'");
    
    this.program[replace_addr] = this.program.push(OpCode.RETURN);
    this.subs[sub_name] = sub_addr;
    
    ++this.token_i;
    return true;
}
