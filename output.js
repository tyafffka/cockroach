/* Модуль вывода
 * -------------
 * 
 * (c) 2019, ТяФ/ка (tyafffka)
 * 
 * Данное программное обеспечение предоставляется "как есть", без каких-либо
 * явных или подразумеваемых гарантий. В любых случаях его автор(ы) снимает(ют)
 * с себя ответственность за любой ущерб, связанный с использованием данного
 * программного обеспечения.
 * 
 * Данное программное обеспечение разрешается использовать кому угодно и в любых
 * целях, включая его коммерческое применение, а также изменять и распространять
 * его свободно при условии соблюдения следующих ограничений:
 * 
 * 1. Происхождение данного программного обеспечения не должно быть искажено; Вы
 *    не в праве утверждать, что это Вы написали оригинальное программное
 *    обеспечение. Если Вы используете данное программное обеспечение в своём
 *    продукте, то упоминание об этом в документации проекта было бы
 *    признательно, но не обязательно.
 * 
 * 2. Изменённые версии исходных файлов должны быть явно обозначены как таковые
 *    и не должны выдаваться за оригинальное программное обеспечение.
 * 
 * 3. Данное уведомление о лицензии не должно убираться или изменяться при любом
 *    распространении исходных текстов. */


/* Набор спрайтов. */
const SpriteType = {
    PLAYER: -1,
    EMPTY: 0,
    BLOCK: 1,
};

/* Набор возможных ориентаций спрайта. */
const SpriteRotation = {
    UP: 0,
    RIGHT: 1,
    DOWN: 2,
    LEFT: 3,
};

/* Размер ячейки игрового поля в пикселях. */
var GAME_CELL_SIZE;

// --- *** ---


/* Убирает информационные сообщения под игровым полем. */
function clearNotes()
{
    completeNote.style.display = "none";
    errorNote.style.display = "none";
}


/* Показывает сообщение о конце игры под игровым полем. */
function writeCompleteNote()
{   completeNote.style.display = "block";   }


/* Показывает сообщение об ошибке под игровым полем. */
function writeErrorNote(msg_)
{
    errorNote.style.display = "block";
    errorNote.innerText = msg_;
    
    console.warn("ERROR NOTE:", msg_);
    return null;
}


/* ОТЛАДКА. Разбирает код из редактора и печатает его в консоле. */
function translateToLog()
{
    console.log(disassembleCode(translateCode(editor.value)));
}


/* Отрисовывает переданное состояние игрового поля. */
function drawState(state_)
{
    let player_x = state_.player_pos[0];
    let player_y = state_.player_pos[1];
    let player_rotation = state_.player_pos[2];
    let map = state_.map;
    
    for(let y = 0; y < map.length; ++y)
    {
        let y_px = GAME_CELL_SIZE * y;
        let row = map[y];
        
        for(let x = 0; x < row.length; ++x)
        {
            let x_px = GAME_CELL_SIZE * x;
            
            if(x == player_x && y == player_y)
                _draw_sprite(SpriteType.PLAYER, x_px, y_px, player_rotation);
            else
                _draw_sprite(row[x], x_px, y_px, SpriteRotation.UP);
        }
    }
    // TODO optimisation: draw only difference
}

// --- *** ---


function _draw_sprite(type_, x_px_, y_px_, rotation_)
{
    let sprite;
    switch(type_)
    {
    case SpriteType.PLAYER: sprite = spritePlayer; break;
    case SpriteType.EMPTY:  sprite = spriteEmpty;  break;
    case SpriteType.BLOCK:  sprite = spriteBlock;  break;
    default:
        console.warn("Unknown sprite " + type_ + " draw at (" + x_px_ + ", " + y_px_ + ")!");
        return;
    }
    
    if(rotation_ == SpriteRotation.UP)
    {   outputContext.drawImage(sprite, x_px_, y_px_);   }
    else
    {
        outputContext.translate(x_px_ + GAME_CELL_SIZE / 2, y_px_ + GAME_CELL_SIZE / 2);
        outputContext.rotate(rotation_ * Math.PI * 0.5);
        outputContext.drawImage(sprite, -(GAME_CELL_SIZE / 2), -(GAME_CELL_SIZE / 2));
        outputContext.setTransform(1, 0, 0, 1, 0, 0);
    }
}
